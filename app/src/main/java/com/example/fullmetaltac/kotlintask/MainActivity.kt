package com.example.fullmetaltac.kotlintask

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val user = User("Dmytro", 25).also { it.pet = Pet("doggy") }

        user.pet?.let {  callPet(it) }

        with(user){
            this.phone = "+38095593856"
            this.address= "Botanical garden"
        }

        foo(i = 555)

        var convertedList = listOf(1, 2, 3).map { it.toString() }
    }

    object clickListener :View.OnClickListener {
        override fun onClick(p0: View?) {
            Log.d(MainActivity::class.java.simpleName, "clicked")
        }

    }
}