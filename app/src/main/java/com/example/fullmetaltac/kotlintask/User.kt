package com.example.fullmetaltac.kotlintask

data class User(private val name: String, private val age: Int) {
    var pet: Pet? = null
    var address: String? = null
    var phone: String? = null
}