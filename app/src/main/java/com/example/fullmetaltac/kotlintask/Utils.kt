package com.example.fullmetaltac.kotlintask

import android.util.Log
import android.view.View
import android.webkit.WebView

fun callPet(pet : Pet){
    Log.d("@UTIL", pet.name)
}

fun foo(s:String = "string", i:Int = -1){
    Log.d("@UTIL", s + i)
}

fun View.makeInvisible(){
    this.visibility = View.GONE
}

fun makeInvisible(vararg views: View){
    views.forEach { it.visibility = View.GONE }
}

fun higherOrderFunction(func: () -> Unit){
    func.invoke()
}

inline fun inlineFunction(lambda: () -> Unit) {
    foo(s = "Inline")
    lambda()
    foo(i = 1)
}

infix fun Int.add(x: Int): Int { return this + x }